import mongoose, {Schema, model} from 'mongoose';

export interface Event extends mongoose.Document {
  tittle:string;
  notes:string;
  start: Date;  
  end: Date;
  user: Schema.Types.ObjectId
}

const EventSchema = new Schema({
  tittle: {
    type: String,
    required: true
  },
  notes: {
    type: String,    
  },
  start: {
    type: Date,
    required: true
  },
  end: {
    type: Date,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});

// OJO CON ESTO 
// EventSchema.method('toJSON', function() {
  
//   const { __v, _id, ...object } = this.toObject();
//   object.id = _id;
//   return object;

// })

export default model<Event>('Event', EventSchema);

