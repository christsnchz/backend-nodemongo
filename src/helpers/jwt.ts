import jwt from 'jsonwebtoken';

export const generateJWT = (uid: any, name: any) => {
  
  return new Promise((resolve, reject) => { 

    const payload = { uid, name };
    jwt.sign(payload, `${process.env.SECRET_JWT_SEED}`, {
      expiresIn: '24h'
    }, (err, token) => {
        
        if (err) {
          reject('error in create token')
        }   
        
        resolve(token);
    });
    

  });

}