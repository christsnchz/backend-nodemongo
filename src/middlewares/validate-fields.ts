import { Response, Request, NextFunction } from 'express';
import { validationResult } from 'express-validator';


export const validatefield = (req: Request, resp: Response, next: NextFunction) => { 
  
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return resp.status(400).json({
      ok: false,
      errors: errors.mapped()
    });    
  }

  next();
}