import { Response, Request, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export const validateJWT = ( req:any , res:Response, next: NextFunction ) => { 


  const token = req.header('x-token');

  if (!token) {
    return res.status(401).json({
      ok: false,
      msg: 'there is no token in the request'
    });
  }


  try {

    const payload = jwt.verify(
      token,
      `${process.env.SECRET_JWT_SEED}`
    );

    console.log('revisa aqui', payload);

    // req.uid = payload.uid;
    // req.name = payload.name;

    
  } catch (error) {

    return res.status(401).json({
      ok: false,
      msg: 'invalid token'
    });
    
  }


  next();

}