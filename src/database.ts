import mongoose from 'mongoose';

export const dbConecction = async () => {
  
  try {

    mongoose.connect( `${process.env.DB_CNN}`,{
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    });

    console.log('DB online');


  } catch (error) {
    console.log(error);
    throw new Error('Error in the connection to DB');


  }

}