
import { Request, Response } from 'express';
import User from '../models/userModel';
import bcrypt from 'bcryptjs';
import { generateJWT } from '../helpers/jwt';

export const newUser = async (req: Request, res: Response) => {

  const { email, password } = req.body;
  
  try {

    let user = await User.findOne({ email });

    if (user) {
      return res.status(400).json({
        ok: false,
        msg: 'the user already exists with that email'
      })
    }

    user = new User(req.body);  
    const salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(password, salt);

    await user.save();

    // generate token
    const token = await generateJWT(user.id, user.name);
    
    res.status(201).json({
      ok: true,
      uid: user.id,
      name: user.name,
      token
    });
    
  } catch (error) {

    res.status(500).json({
      ok: false,
      msg: 'Error from DB'
    })    
  }
  

}

export const loginUser = async (req: Request, res: Response) => {

  try {
    const { email, password } = req.body;
    
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(400).json({
        ok: false,
        msg: 'the user do not exits'
      })
    }

    const validPass = bcrypt.compareSync(password, user.password);

    if (!validPass) {
      return res.status(400).json({
        ok: false,
        msg: 'incorrect credentials'
      });
    }

    // generate token
    const token = await generateJWT(user.id, user.name);


    res.json({
      ok: true,
      uid: user.id,
      name: user.name,
      token
    });

    
  } catch (error) {

    res.status(500).json({
      ok: false,
      msg: 'Error from DB'
    });        
  }
  
}

export const checkToken = async (req: Request, res: Response) => {
  
  // const uid = req.uid;
  // const name = req.name;

  // const token = await generateJWT(user.id, user.name);

  res.json({
    ok: true,
    
  });

}