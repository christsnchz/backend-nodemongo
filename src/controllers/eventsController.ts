import { Request, Response } from 'express';
import Event from '../models/EventoModel';

export const getEventos = async (req: any, res: Response) => {
  
  const eventos = await Event.find()
                              .populate('user', 'name');

  res.json({
    ok: true,
    eventos
  });  
}

export const createEvent = async (req: any, res: Response) => {

  const event = new Event(req.body);
  try {

    event.user = req.uid
    const result = await event.save();

    res.json({
      ok: true,
      msg: result
    });  
    
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg:' error'
    });    
  }    
  
}

export const updateEvent = async (req: any, res: Response) => {

  const eventId = req.params.id;
  const uid = req.uid; 

  try {

    const event = await Event.findById(eventId);

    if (!event) {      
      return res.status(404).json({
        ok: false,
        msg:' element not found'
      });      
    }

    if (event?.user.toString() !== uid) {
      return res.status(401).json({
        ok: false,
        msg:' prohibido'
      });      
    }

    const newEvent = {
      ...req.body,
      user: uid
    }

    const result = await Event.findByIdAndUpdate(eventId, newEvent, {new: true});    
    res.json({
      ok: true,
      newEvent
    });
    
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg:'Error in BD'
    });
    
  }  
}

export const deleteEvent = async (req: any, res: Response) => {

  const eventId = req.params.id;
  const uid = req.uid; 

  try {

    const event = await Event.findById(eventId);
    if (!event) {      
      return res.status(404).json({
        ok: false,
        msg:' element not found'
      });      
    }

    if (event?.user.toString() !== uid) {
      return res.status(401).json({
        ok: false,
        msg:' prohibido'
      });      
    }
  
    await Event.findByIdAndDelete(eventId);    
    
    res.json({
      ok: true,      
    });
    
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg:'Error in BD'
    });
    
  }  
  
}