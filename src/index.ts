import express from 'express';
import dotenv from 'dotenv';
import authRouter from './routes/auth';
import eventsRouter from './routes/events';
import { dbConecction } from './database';
import cors from 'cors';
dotenv.config();


//Crear el servidor
const app = express();

//Data base
dbConecction();

/** CORS */
app.use(cors() )


//Directorio Publico
app.use(express.static('public'));


//Middlewares
app.use(express.json());

//routes
app.use('/api/auth', authRouter);
app.use('/api/events', eventsRouter );


app.listen(process.env.PORT, () => {
  console.log(`server corriendo en ${process.env.PORT}`);
})