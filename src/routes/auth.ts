import { Router } from 'express';
import { check } from 'express-validator';
const authRouter: Router = Router();
import { newUser, loginUser, checkToken } from '../controllers/authController';
import { validatefield } from '../middlewares/validate-fields';
import { validateJWT } from '../middlewares/validate-jwt';


authRouter.post(
  '/new',
  [
    check('name', 'el nombre es obligatorio').not().isEmpty(),
    check('password', 'el password debe ser de 6 caracteres').isLength({ min: 6}),
    check('email', 'el email es obligatorio').isEmail(),
    validatefield
  ],
  newUser);

authRouter.post(
  '/',
  [    
    check('password', 'el password debe ser de 6 caracteres').isLength({ min: 6}),
    check('email', 'el email es obligatorio').isEmail(),
    validatefield
  ],
  loginUser);

authRouter.get('/renew', validateJWT ,checkToken );


export default authRouter;