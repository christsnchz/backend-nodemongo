import { Router } from 'express';
import { check } from 'express-validator';
import { getEventos, createEvent, updateEvent, deleteEvent } from '../controllers/eventsController';
import { isDate } from '../helpers/isDate';
const eventsRouter: Router = Router();
import { validatefield } from '../middlewares/validate-fields';
import { validateJWT } from '../middlewares/validate-jwt';

eventsRouter.use( validateJWT );

eventsRouter.get('', getEventos );

eventsRouter.post('/',
  [
    check('tittle', 'el titulo es obligatorio').not().isEmpty(),
    check('start', 'Fecha de inicio es obligatoria').custom(isDate),
    check('end', 'Fecha de inicio es obligatoria').custom(isDate),
    validatefield
    
  ],
  createEvent);

eventsRouter.put('/:id', updateEvent );

eventsRouter.delete('/:id', deleteEvent );

export default eventsRouter;